import $ from 'jquery';
import { paises } from '../dados-paises';
import { boutiques } from '../dados-boutiques';
import { MarkerClusterer } from './markerclusterer';

function encontrarBoutique() {

    const btnBuscar = $('#btnBuscar');
    const inputEncontre = $('input[name="encontre"]');
    const selectPais = $('.boutique-select #pais');
    const selectCidade = $('.boutique-select #cidade');
    const btnVoltar = $('#link-voltar');
    let count;
    let countLojas;
    let countAutorizadas;
    let idPais;
    let idCidade;
    let idEncontre;
    let nameCidade;
    let html;
    let htmlBoutique;
    let htmlAutorizadas;

    function ableBoutiqueInputs() {
        $('#lojas-oficiais').prop('disabled', false);
        $('#lojas-oficiais').prop('checked', false);
        $('.lojas-oficiais').removeClass('disabled');
        $('#vendedores').prop('disabled', false);
        $('#vendedores').prop('checked', false);
        $('.vendedores').removeClass('disabled');
    }

    function criarSelect(selectName, countryNameId) {
        paises.countries.map(function (x) {
            if (selectName === "pais") {
                selectPais.append(`
                    <option value="${x.countryNameId.id}">${x.countryNameId.name}</option>
                `);
            }
            if (x.countryNameId.id === countryNameId) {
                x.cities.map(function (x) {
                    selectCidade.append(`
                        <option value="${x.id}">${x.name}</option>
                    `);
                })
            }
            if (countryNameId === "null") {
                selectCidade.html('');
            }
        });
    }

    function ordenarSelect(id) {
        var selectToSort = $('.boutique-select #' + id);
        var optionActual = selectToSort.val();
        selectToSort.html(selectToSort.children('option').sort(function (a, b) {
            return a.text === b.text ? 0 : a.text < b.text ? -1 : 1;
        })).val(optionActual);
    }

    function prependSelect(selectName, value) {
        selectName.prepend(`
            <option selected="true">${value}</option>
        `);
    }

    function initialize(idP, idC, idE) {
        let arrayBoutiques = []
        boutiques.branches.map(function (x) {
            try {
                if (idE === "1") {
                    if (x.country.id === idP & x.city.id === idC & x.type.id !== "3") {
                        arrayBoutiques.push({ "Latitude": x.latitude, "Longitude": x.longitude });
                    }
                } else if (idE === "2") {
                    if (x.country.id === idP & x.city.id === idC & x.type.id === "3") {
                        arrayBoutiques.push({ "Latitude": x.latitude, "Longitude": x.longitude });
                    }
                }
            } catch (e) {

            }
        })

        let latid = arrayBoutiques[0].Latitude;
        let longi = arrayBoutiques[0].Longitude;
        let centerLongi = parseFloat(longi) - 0.10;
        centerLongi.toString();

        var mapOptions = {
            center: new google.maps.LatLng(latid, centerLongi),
            zoom: 11,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true,
            zoomControl: true
        };
        var map = new google.maps.Map(document.getElementById("map_canvas"),
            mapOptions);

        var markers = arrayBoutiques.map(function (ponto, i) {
            return new google.maps.Marker({
                position: new google.maps.LatLng(ponto.Latitude, ponto.Longitude),
                map: map,
                icon: './arquivos/icon-marcador.png'
            });
        });

        new MarkerClusterer(map, markers, { imagePath: './arquivos/m' });
    }

    criarSelect('pais', null);
    ordenarSelect('pais');
    prependSelect(selectPais, "Selecione o País");
    prependSelect(selectCidade, "Selecione a Cidade");

    selectPais.on('change', function () {
        ableBoutiqueInputs();
        idPais = $(this).val()
        selectCidade.html('');
        criarSelect('cidade', idPais);
        ordenarSelect('cidade');
        prependSelect(selectCidade, "Selecione a Cidade");
        idCidade = undefined;
        idEncontre = undefined;
    });

    selectCidade.on('change', function () {
        count = 0;
        countLojas = 0;
        countAutorizadas = 0;
        htmlBoutique = '';
        htmlAutorizadas = '';
        ableBoutiqueInputs();
        idCidade = $(this).val();
        idEncontre = undefined;
        boutiques.branches.map(function (x) {
            let validaInfo = true;
            if (x.time_open_1 === "" & x.time_open_2 === "" & x.time_open_3 === "" & x.time_open_4 === "") {
                validaInfo = false;
            }
            html = `
                <div class="enderecosText" id="${count}">
                    <h1>${x.name}</h1>
                    <h2>${x.adress_1}</h2>
                    <h2>${x.adress_2}</h2>
                    <h2>${x.adress_3}</h2>
                    <h2>${x.adress_4}</h2>
                    ${x.phone === "" ? `` : `<h2>Tel:<span>${x.phone}</span></h2>`}
                    <div class="time-open">
                        <h1>${x.time_open_1}</h1>
                        <h1>${x.time_open_2}</h1>
                        <h1>${x.time_open_3}</h1>
                        <h1>${x.time_open_4}</h1>
                    </div>
                    ${validaInfo ? `<div class="maisInformacoes" id="${count}">Mais Informações<i class='fas fa-chevron-down'></i><i class='fas fa-chevron-up active'></i></div>` : ``}
                    ${validaInfo ? `<div class="maisInformacoes disabled" id="${count}">Fechar<i class='fas fa-chevron-down'></i><i class='fas fa-chevron-up active'></i></div>` : ``}
                </div>
            `;
            try {
                if (x.city.id === idCidade) {
                    if (x.type.id === "1" || x.type.id === "2") {
                        countLojas++;
                        count++;
                        htmlBoutique += html;
                    } else if (x.type.id === "3") {
                        countAutorizadas++;
                        count++;
                        htmlAutorizadas += html;
                    }
                    nameCidade = x.city.name;
                }
            } catch (e) {

            }
        })
        if (countLojas < 1) {
            $('#lojas-oficiais').prop('disabled', true);
            $('.lojas-oficiais').addClass('disabled');
        }
        if (countAutorizadas < 1) {
            $('#vendedores').prop('disabled', true);
            $('.vendedores').addClass('disabled');
        }
    });

    inputEncontre.click(function () {
        idEncontre = $(this).val();
        if (idEncontre === '1') {
            $('#countBusca').text(`
                ${ countLojas < 2
                    ? `${countLojas} Resultado de Busca`
                    : `${countLojas} Resultados de Busca`
                }
            `)
            $('.map .enderecos .main').html('');
            $('.map .enderecos .main').append(htmlBoutique);
        }
        if (idEncontre === '2') {
            $('#countBusca').text(`
                ${ countAutorizadas < 2
                    ? `${countAutorizadas} Resultado de Busca`
                    : `${countAutorizadas} Resultados de Busca`
                }
            `)
            $('.map .enderecos .main').html('');
            $('.map .enderecos .main').append(htmlAutorizadas);
        }
        initialize(idPais, idCidade, idEncontre);
        $('.maisInformacoes').click(function () {
            var id = $(this).attr('id');
            $('.main #' + id + ' .time-open').slideToggle();
            $('.main #' + id + ' .maisInformacoes').toggleClass('disabled');
            $('#' + id + ' .fa-chevron-up').toggleClass('active');
            $('#' + id + ' .fa-chevron-down').toggleClass('active');
        })
    })

    btnBuscar.click(function () {
        if (idPais == undefined || idCidade == undefined || idEncontre == undefined) {
            $('#errorPreenchimento').text('*Preencha todos os campos');
            return;
        }
        $('.encontrar-boutique form').appendTo('.map .content .enderecos .topo');
        $('#topo-title').text(nameCidade);
        $('.encontrar-boutique').hide();
        $('.map').show();
        initialize(idPais, idCidade, idEncontre);
        $('#errorPreenchimento').text('');
    })

    btnVoltar.click(function () {
        $('.map .content .enderecos .topo form').appendTo('.encontrar-boutique .content');
        $('.encontrar-boutique').show();
        $('.map').hide();
    })
}

export default encontrarBoutique;
