// WARNING: THE USAGE OF CUSTOM SCRIPTS IS NOT SUPPORTED. VTEX IS NOT LIABLE FOR ANY DAMAGES THIS MAY CAUSE. THIS MAY BREAK YOUR STORE AND STOP SALES. IN CASE OF ERRORS, PLEASE DELETE THE CONTENT OF THIS SCRIPT.

$(window).on('orderFormUpdated.vtex', function() {
    setTimeout(function() {

    $('head').append('<link href="https://cdn.lineicons.com/1.0.1/LineIcons.min.css" rel="stylesheet">');
    $('head').append('<link href="/arquivos/font-montblanc.css" rel="stylesheet" type="text/css" />');
    $('#cartLoadedDiv th.product-price').text('Preço unitário');
    $('#cartLoadedDiv th.quantity-price').text('Subtotal');
    $('#shipping-calculate-link').trigger('click');
    $('#cart-link-coupon-add').trigger('click');
    $('.clearfix.pull-right.cart-links.cart-links-bottom.hide').appendTo('.summary-template-holder .row-fluid.summary');
    $('#shipping-preview-container h2.srp-main-title').text('Resumo da Compra');



    setTimeout(function() {
        $('p.input.ship-postalCode.required.text label').text('Calcular entrega - digite seu CEP');
      $('.coupon.summary-coupon .coupon-label label').text('Tem um cupom de desconto?');
      $('#cart-shipping-calculate').html('<i class="lni-arrow-right input-cep"></i>');
      $('button#cart-coupon-add').html('<i class="lni-arrow-right input-cep"></i>');
      $('#ship-postalCode').attr('placeholder','Insira seu CEP');
      $('input#cart-coupon').attr('placeholder','Insira aqui seu código de desconto');
      $('.span5.totalizers.summary-totalizers.cart-totalizers.pull-right .accordion-group .table tfoot tr .info').text('Total da compra');
      $('a#cart-to-orderform').text('FINALIZAR COMPRA');
      $('a#cart-choose-more-products').text('Continuar comprando');
      $('a#cart-choose-more-products').prepend('<i class="lni-arrow-right"></i>');

    },400);

    }, 400);
});
