## GRUPO VITORELLI - Ecommerce de Profissionais
##### Este documento tem com objetivo, instrui-lo na instalção do projeto

------------

### instalação de libs e softwares

> - NodeJS

Baixe o nodejs em sua máquina - link abaixo
[NodeJS](https://nodejs.org/en/ "NodeJS")

> - Yarn

Baixe o Yarn em sua máquina - link abaixo
[Yarn](https://yarnpkg.com/pt-BR/ "Yarn")

> - Ruby Installer no Windows

https://github.com/oneclick/rubyinstaller2/releases/download/RubyInstaller-2.6.3-1/rubyinstaller-devkit-2.6.3-1-x64.exe

Marque todas as caixas quando fazer a instalação

Após a instalação, seu computudar tera um terminal ruby. Abra o menu e procure start terminal with ruby

- Rode 
**gem install sass**

- Rode
**gem install compass**

### Rodando o projeto para desenvolvimento

Abra seu terminal na pasta onde se encontra o arquivo **package.json**
rode o comando em seu console: **yarn start**
rode o comando em seu console: **yarn sass** em uma nova aba

### Rodando o projeto para produção

Abra seu terminal na pasta onde se encontra o arquivo **package.json**
rode o comando em seu console: **yarn build**

### INFORMAÇÃO

Todo seu css deve ser feito na pasta sass dentro de public. Já consta uma arquitetura pronto e tem alguns exemplos dentro de components.