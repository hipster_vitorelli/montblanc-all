export const topicos = {
    pedidos: { // TÓPICO ******************************************
        nome: "Meus Pedidos",
        itens: [
            {
                titulo: "Pagamento",
                pergunta_resposta: [
                    {
                        pergunta: `Eu vou receber uma nota fiscal referente ao meu pedido?`,
                        resposta: `Sim, uma nota fiscal é enviada para cada produto comprado na Loja Montblanc. Este documento virá anexado ao seu e-mail de confirmação de compra como um arquivo PDF. Caso você não receva ou perca o e-mail, ficaremos felizes em reenviar o documento.<br><br>
                        Por favor entre em contato com o time de Relacionamento com o Cliente Montblanc através do telefone ou e-mail para solicitar o reenvio do arquivo da sua nota fiscal.`
                    },
                    {
                        pergunta: `A minha entrega virá com uma nota fiscal?`,
                        resposta: `Resposta 02`
                    }
                ]
            },
            {
                titulo: "Envios & entregas",
                pergunta_resposta: [
                    {
                        pergunta: `Pergunta 1`,
                        resposta: `Resposta 1`
                    },
                    {
                        pergunta: `Pergunta 2`,
                        resposta: `Resposta 2`
                    }
                ]
            },
            {
                titulo: "Trocas & devoluções",
                pergunta_resposta: [
                    {
                        pergunta: `O que está incluído na garantia dos produtos?`,
                        resposta: `Resposta 1`
                    },
                    {
                        pergunta: `Pergunta 2`,
                        resposta: `Resposta 2`
                    },
                    {
                        pergunta: `Pergunta 3`,
                        resposta: `Resposta 3`
                    }
                ]
            }
        ]
    },
    online: { // TÓPICO ******************************************
        nome: "Serviços Online",
        itens: [
            {
                titulo: "Título 1",
                pergunta_resposta: [
                    {
                        pergunta: `Pergunta 1`,
                        resposta: `Resposta 1`
                    },
                    {
                        pergunta: `Pergunta 2`,
                        resposta: `Resposta 2`
                    },
                    {
                        pergunta: `Pergunta 3`,
                        resposta: `Resposta 3`
                    }
                ]
            },
            {
                titulo: "Título 2",
                pergunta_resposta: [
                    {
                        pergunta: `Pergunta 1`,
                        resposta: `Resposta 1`
                    },
                    {
                        pergunta: `Pergunta 2`,
                        resposta: `Resposta 2`
                    },
                    {
                        pergunta: `Pergunta 3`,
                        resposta: `Resposta 3`
                    }
                ]
            }
        ]
    },
    lojas: { // TÓPICO ******************************************
        nome: "Serviços Lojas Físicas",
        itens: [
            {
                titulo: "Título 3",
                pergunta_resposta: [
                    {
                        pergunta: `Pergunta 1`,
                        resposta: `Resposta 1`
                    },
                    {
                        pergunta: `Pergunta 2`,
                        resposta: `Resposta 2`
                    },
                    {
                        pergunta: `Pergunta 3`,
                        resposta: `Resposta 3`
                    }
                ]
            },
            {
                titulo: "Título 4",
                pergunta_resposta: [
                    {
                        pergunta: `Pergunta 1`,
                        resposta: `Resposta 1`
                    },
                    {
                        pergunta: `Pergunta 2`,
                        resposta: `Resposta 2`
                    },
                    {
                        pergunta: `Pergunta 3`,
                        resposta: `Resposta 3`
                    }
                ]
            }
        ]
    },
    produtos: { // TÓPICO ******************************************
        nome: "Produtos",
        itens: [
            {
                titulo: "Título 5",
                pergunta_resposta: [
                    {
                        pergunta: `Como eu preencho a minha caneta tinteiro com tinta?`,
                        resposta: `Resposta 1`
                    },
                    {
                        pergunta: `Pergunta 2`,
                        resposta: `Resposta 2`
                    },
                    {
                        pergunta: `Pergunta 3`,
                        resposta: `Resposta 3`
                    }
                ]
            },
            {
                titulo: "Título 6",
                pergunta_resposta: [
                    {
                        pergunta: `Pergunta 1`,
                        resposta: `Resposta 1`
                    },
                    {
                        pergunta: `Pergunta 2`,
                        resposta: `Resposta 2`
                    },
                    {
                        pergunta: `Pergunta 3`,
                        resposta: `Resposta 3`
                    }
                ]
            }
        ]
    },
    reparos: { // TÓPICO ******************************************
        nome: "Reparos & Serviços",
        itens: [
            {
                titulo: "Título 7",
                pergunta_resposta: [
                    {
                        pergunta: `Como posso conseguir o valor aproximado de um serviço?`,
                        resposta: `Resposta 1`
                    },
                    {
                        pergunta: `Pergunta 2`,
                        resposta: `Resposta 2`
                    },
                    {
                        pergunta: `Pergunta 3`,
                        resposta: `Resposta 3`
                    }
                ]
            },
            {
                titulo: "Título 8",
                pergunta_resposta: [
                    {
                        pergunta: `Pergunta 1`,
                        resposta: `Resposta 1`
                    },
                    {
                        pergunta: `Pergunta 2`,
                        resposta: `Resposta 2`
                    },
                    {
                        pergunta: `Pergunta 3`,
                        resposta: `Resposta 3`
                    }
                ]
            }
        ]
    },
    refis: { // TÓPICO ******************************************
        nome: "Encontrar Refis",
        itens: [
            {
                titulo: "Título 9",
                pergunta_resposta: [
                    {
                        pergunta: `De qual refil eu preciso?`,
                        resposta: `Resposta 1`
                    },
                    {
                        pergunta: `A Loja Montblanc possui refis para lapiseiras?`,
                        resposta: `Resposta 2`
                    },
                    {
                        pergunta: `Pergunta 3`,
                        resposta: `Resposta 3`
                    }
                ]
            },
            {
                titulo: "Título 10",
                pergunta_resposta: [
                    {
                        pergunta: `Pergunta 1`,
                        resposta: `Resposta 1`
                    },
                    {
                        pergunta: `Pergunta 2`,
                        resposta: `Resposta 2`
                    },
                    {
                        pergunta: `Pergunta 3`,
                        resposta: `Resposta 3`
                    }
                ]
            },
            {
                titulo: "Título 11",
                pergunta_resposta: [
                    {
                        pergunta: `Pergunta 1`,
                        resposta: `Resposta 1`
                    },
                    {
                        pergunta: `Pergunta 2`,
                        resposta: `Resposta 2`
                    },
                    {
                        pergunta: `Pergunta 3`,
                        resposta: `Resposta 3`
                    }
                ]
            }
        ]
    }
};
