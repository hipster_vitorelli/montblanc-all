import $ from 'jquery';

function contato() {
    $("input#cpf").mask("999.999.999-99");
    $("input#nome").attr('maxlength', 100);
    $('input#telefone').mask('(00)00000-0009');
    $('input#telefone').blur(function () {
        if ($(this).val().length == 12) {
            $('input#telefone').mask('(00)000-000009');
        } else {
            $('input#telefone').mask('(00)00000-0009');
        }
    });
}

export default contato;
