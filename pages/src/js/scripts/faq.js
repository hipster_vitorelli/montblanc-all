import $ from 'jquery';
import { topicos } from '../dados-faq';

function faq() {
    // CRIAR OS ITENS DOS TÓPICOS
    let qtd = 0;
    for (let item in topicos) {
        $('.result-topicos').append(`
            <div class="result-topico" id="${item}" style="display: none;">
                <div class="result-topico-box1"></div>
                <div class="result-topico-box2">
                    <div class="buscadas">
                        <h1>Perguntas mais buscadas</h1>
                        <div class="buscadas-conteudo">
                            <a href="#" class="click-pergunta" id="${item}-0-0">${topicos[item].itens[0].pergunta_resposta[0].pergunta}</a>
                            <a href="#" class="click-pergunta" id="${item}-1-0">${topicos[item].itens[1].pergunta_resposta[0].pergunta}</a>
                        </div>
                    </div>
                </div>
            </div>
        `);
        let div = $('.result-topicos #' + item + ' .result-topico-box1');
        $(div).append(`
    <div class="links">
        <i class="lni-arrow-left voltar"></i>
        <span class="voltar">Voltar</span>
        <span class="voltar">Tópicos</span>
        <span>${topicos[item].nome}</span>
    </div>
    `);
        for (let i = 0; i < topicos[item].itens.length; i++) {
            let links = '';
            for (let j = 0; j < topicos[item].itens[qtd].pergunta_resposta.length; j++) {
                links += `
                <a href="#" class="click-pergunta" id="${item + '-' + i + '-' + j}">${topicos[item].itens[qtd].pergunta_resposta[j].pergunta}</a>
            `;
            }
            let html = `
                <div class="perguntas" id="perguntas${qtd + 1}">
                    <div class="perguntas-title" id="perguntas${qtd + 1}">${topicos[item].itens[qtd].titulo}</div>
                    <div class="perguntas-icon">
                        <span class="icon-column"></span>
                        <span class="icon-row"></span>
                    </div>
                    <div class="perguntas-links">
                        <div class="box-links">
                            ${links}
                        </div>
                    </div>
                </div>
            `;
            $(div).append(html);

            qtd++
        }
        qtd = 0;
    }

    // PERGUNTAS MAIS BUSCADAS
    let html = '';
    html += `
        <a href="#" class="click-pergunta" id="refis-0-0">${topicos.refis.itens[0].pergunta_resposta[0].pergunta}</a>
        <a href="#" class="click-pergunta" id="pedidos-2-0">${topicos.pedidos.itens[2].pergunta_resposta[0].pergunta}</a>
        <a href="#" class="click-pergunta" id="produtos-0-0">${topicos.produtos.itens[0].pergunta_resposta[0].pergunta}</a>
        <a href="#" class="click-pergunta" id="reparos-0-0">${topicos.reparos.itens[0].pergunta_resposta[0].pergunta}</a>
        <a href="#" class="click-pergunta" id="refis-0-1">${topicos.refis.itens[0].pergunta_resposta[1].pergunta}</a>
    `;
    $('#mais_buscadas').append(html);

    function btnVoltar() {
        $('.voltar').click(function () {
            $('.topicos-buscadas').show();
            $('.result-topico').hide();
            $('.perguntas-links').hide();
            $('.result-respostas').hide();
            $('.result-busca').hide();
            $('.perguntas-icon .icon-column').removeClass('active');
            $('.result-respostas').html('');
        })
    };

    function clickPergunta() {
        $('.click-pergunta').click(function () {
            $('.result-respostas').html('');
            $('.topicos-buscadas').hide();
            $('.perguntas-links').hide();
            $('.result-topicos').hide();
            $('.result-busca').hide();
            $('.perguntas-icon .icon-column').removeClass('active');
            $('.result-respostas').show();
            var id = $(this).attr('id');
            var busca = id.split('-');
            let html = `
                <div class="links">
                    <i class="lni-arrow-left voltar"></i>
                    <span class="voltar">Voltar</span>
                    <span class="voltar">Tópicos</span>
                    <span class="btn-topico" id="${busca[0]}">${topicos[busca[0]].nome}</span>
                    <span>${topicos[busca[0]].itens[busca[1]].titulo}</span>
                </div>
                <div class="pergunta">
                    ${topicos[busca[0]].itens[busca[1]].pergunta_resposta[busca[2]].pergunta}
                </div>
                <div class="resposta">
                    ${topicos[busca[0]].itens[busca[1]].pergunta_resposta[busca[2]].resposta}
                </div>
                <div class="util">
                    <h1>Esta resposta foi útil?</h1>
                    <div class="util-btn">
                        <div class="btn" id="util-sim">SIM</div>
                        <div class="btn" id="util-nao">NÃO</div>
                    </div>
                </div>
                <div class="fale-conosco">
                    <h1>Caso necessite de outras informações por favor entre em contato.</h1>
                    <a href="#" class="fale-conosco-btn">FALE CONOSCO</a>
                </div>
            `;
            $('.result-respostas').append(html);

            $('.links .btn-topico').click(function () {
                var id = $(this).attr('id');
                console.log(id);
                $('.result-respostas').hide();
                $('.result-topicos').show();
                $('.result-topicos #' + id).show();
                $('.result-respostas').html('');
            })
            btnVoltar();

        })
    }

    clickPergunta();

    $('.topico').click(function () {
        var id = $(this).attr('id');
        $('.topicos-buscadas').hide();
        $('.result-topicos').show();
        $('.result-topicos #' + id).show();
    })

    btnVoltar();

    $('.perguntas-title').click(function () {
        var id = $(this).attr('id');
        $('#' + id + ' .perguntas-links').slideToggle();
        $('#' + id + ' .perguntas-icon .icon-column').toggleClass('active');
    })

    function executaBusca() {
        $('.result-busca').html('');
        $('.topicos-buscadas').hide();
        $('.result-topicos').hide();
        $('.result-respostas').hide();
        $('.result-busca').show();

        function tiraAcentos(i) {
            var i = i.toLowerCase().trim();
            var acentos = "ãáàâäéèêëíìîïõóòôöúùûüç";
            var sem_acentos = "aaaaaeeeeiiiiooooouuuuc";
            for (var x = 0; x < i.length; x++) {
                var str_pos = acentos.indexOf(i.substr(x, 1));
                if (str_pos != -1) {
                    i = i.replace(acentos.charAt(str_pos), sem_acentos.charAt(str_pos));
                }
            }
            return i;
        }

        var buscaTermo = $('#text-busca').val();

        let count = 0;
        let links = `
            <div class="links">
                <i class="lni-arrow-left voltar"></i>
                <span class="voltar">Voltar</span>
                <span class="voltar">Tópicos</span>
            </div>
            <h1>Respostas encontradas para sua busca</h1>
        `;
        $('.result-busca').append(links);
        for (let item in topicos) {
            for (let i = 0; i < topicos[item].itens.length; i++) {
                var html = topicos[item].itens[i].titulo.trim();
                var html_limpo = tiraAcentos(html);
                var pattern = "([^\\w]*)(" + tiraAcentos(buscaTermo) + "|" + buscaTermo + ")([^\\w]*)";
                var rg = new RegExp(pattern, "g");
                var match = rg.exec(html_limpo);

                if (match) {
                    count++;
                    let links2 = `
                        <h1>${topicos[item].nome} - ${topicos[item].itens[i].titulo}</h1>
                        <div class="box-links"></div>
                    `;
                    $('.result-busca').append(links2);
                    for (let j = 0; j < topicos[item].itens[i].pergunta_resposta.length; j++) {
                        links2 = `
                            <a href="#" class="click-pergunta" id="${item + '-' + i + '-' + j}">${topicos[item].itens[i].pergunta_resposta[j].pergunta}</a>
                        `;
                        $('.result-busca .box-links').append(links2);
                    }
                }
            }
        }
        links = `
            <div class="fale-conosco">
                <h1>Caso necessite de outras informações por favor entre em contato.</h1>
                <a href="#" class="fale-conosco-btn">FALE CONOSCO</a>
            </div>
        `;
        $('.result-busca').append(links);
        if (count == 0) {
            links = `
                <div class="links">
                    <i class="lni-arrow-left voltar"></i>
                    <span class="voltar">Voltar</span>
                    <span class="voltar">Tópicos</span>
                </div>
                <h1>Não foi possível encontrar respostas para sua busca</h1>
                <div class="fale-conosco">
                    <h1>Caso necessite de outras informações por favor entre em contato.</h1>
                    <a href="#" class="fale-conosco-btn">FALE CONOSCO</a>
                </div>
            `;
            $('.result-busca').html('');
            $('.result-busca').append(links);
        }
        btnVoltar();
        clickPergunta();
    };
    $('#btn-busca').click(function () {
        executaBusca();
    })
    $('#text-busca').keypress(function (e) {
        if (e.which == 13) {
            executaBusca();
        }
    });
}

export default faq;
