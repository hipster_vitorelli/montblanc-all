import $ from 'jquery';
import 'slick-carousel';

function home() {
    $('.lni-plus').click(function () {
        let id = $(this).attr('id');
        let div = '.banner-categorias .content #';
        $(div + id + ' .over-categoria').css('display', 'flex');
        $(div + id + ' .lni-close').css('display', 'block');
        $(div + id + ' .lni-plus').css('display', 'none');
        $(div + id + ' h1').css('display', 'none');
    });

    $('.lni-close').click(function () {
        let id = $(this).attr('id');
        let div = '.banner-categorias .content #';
        $(div + id + ' .over-categoria').css('display', 'none');
        $(div + id + ' .lni-close').css('display', 'none');
        $(div + id + ' .lni-plus').css('display', 'block');
        $(div + id + ' h1').css('display', 'block');
    });

    $('.fullbanner-home').not('.slick-initialized').slick({
        infinite: true,
        speed: 600,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        dots: true,
        autoplay: false,
        prevArrow: `<div class='arrow prev'><i class="lni-arrow-left"></i></div>`,
        nextArrow: `<div class='arrow next'><i class="lni-arrow-right"></i></div>`
    });

    let $classe = $('.banner-product2-home .products ul');
    $classe.find('.helperComplement').remove();
    $classe.not('.slick-initialized').slick({
        infinite: true,
        speed: 600,
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        autoplay: false,
        prevArrow: `<div class='arrow prev'><i class="lni-arrow-left"></i></div>`,
        nextArrow: `<div class='arrow next'><i class="lni-arrow-right"></i></div>`,
        responsive: [
            {
                breakpoint: 950,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
}

export default home;
