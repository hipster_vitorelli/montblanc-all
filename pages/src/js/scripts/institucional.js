import $ from 'jquery';

function institucional() {
    $('.institucional-titles .title').click(function () {
        var id = $(this).attr('id');
        $('.title').removeClass('active');
        $('#' + id).addClass('active');
        $('.institucional-contents .text').removeClass('active');
        $('.institucional-contents #' + id).addClass('active');
    })
}

export default institucional;
