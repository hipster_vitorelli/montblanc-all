import $ from 'jquery';
import 'slick-carousel';

function landingPageModular() {
    $('.fullbanner-landing').not('.slick-initialized').slick({
        infinite: true,
        speed: 600,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        dots: true,
        autoplay: false,
        prevArrow: `<div class='arrow prev'><i class="lni-arrow-left"></i></div>`,
        nextArrow: `<div class='arrow next'><i class="lni-arrow-right"></i></div>`
    });

    $('.fullbanner-landing2').not('.slick-initialized').slick({
        infinite: true,
        speed: 600,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        dots: true,
        autoplay: false,
        prevArrow: `<div class='arrow prev'><i class="lni-arrow-left"></i></div>`,
        nextArrow: `<div class='arrow next'><i class="lni-arrow-right"></i></div>`
    });
}

export default landingPageModular;
