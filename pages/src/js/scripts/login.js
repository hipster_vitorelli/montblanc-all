import $ from 'jquery';

function login() {
    $('.icon.cadastre-se.login').click(function () {
        $('#vtexIdUI-auth-selector .vtexIdUI-heading').first().text('Escolha um método para acessar sua conta');
        $('#vtexIdUI-email-confirmation .vtexIdUI-heading').text('Por favor, informe seu e-mail');
        $('#vtexIdUI-classic-login .modal-footer #classicLoginBtn').text('CONFIRMAR');
        $('#vtexIdUI-classic-login .control-label .dead-link').appendTo('#vtexIdUI-classic-login .modal-body .vtexIdUI-classic-login-control:nth-child(2)');
        $('.vtexIdUI .modal-header').append('<i class="lni-close" id="btn-close-login"></i>');
        $('#vtexIdContainer').slideToggle();
        $('i#btn-close-login').click(function () {
            $('#vtexIdContainer').slideToggle();
        })
    })
}

export default login;
