import $ from 'jquery';
import 'slick-carousel';

function produto() {
    $('.body-product .apresentacao .thumbs').not('.slick-initialized').slick({
        infinite: true,
        speed: 600,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        dots: true,
        autoplay: false,
        prevArrow: `<div class='arrow prev'><i class="lni-arrow-left"></i></div>`,
        nextArrow: `<div class='arrow next'><i class="lni-arrow-right"></i></div>`
    });

    $(".body-product .lista-desejos").click(function () {
        $(".body-product .lista-desejos .icon-wish").toggleClass("active");
    })

    let skuBestPrice = $('.body-product .skuBestPrice').text();
    let DivBestPrice = $('.body-product .price-best-price');
    DivBestPrice.text(skuBestPrice + " à vista");
    // $(".body-product .valor-dividido .skuBestInstallmentValue").append(" com juros");

    let qtdThumbs = $(".apresentacao .thumbs ul li").length;
    let qtdSlide = $('.body-product .qtd-slide');
    qtdSlide.html("1/" + qtdThumbs);

    $('.apresentacao .thumbs').on('afterChange', function (event, slick, currentSlide) {
        let slide = currentSlide + 1;
        qtdSlide.html(slide + "/" + qtdThumbs);
    });

    $(".produto-descricao .links .link").click(function () {
        let id = $(this).attr('id');
        $(".produto-descricao .links .link").removeClass('active');
        $(".produto-descricao .links #" + id).addClass('active');
        $(".produto-descricao .boxs .box-conteudo").removeClass('active');
        $(".produto-descricao .boxs .box-conteudo .texto").removeClass('active');
        $(".produto-descricao .boxs #" + id).addClass('active');
        $(".produto-descricao .boxs .box-conteudo .btn-lerMais").remove();
        $(".produto-descricao .boxs #" + id + ' .fa-chevron-down').removeClass('active');
        $(".produto-descricao .boxs #" + id + ' .fa-chevron-up').addClass('active');

        let qtdLinhas = $(".produto-descricao .boxs #" + id).text().length;
        if (qtdLinhas > 749) {
            $(".produto-descricao .boxs #" + id).append(`
                <div class="btn-lerMais">
                    <p>Ler Mais</p>
                    <i class='fas fa-chevron-down'></i>
                    <i class='fas fa-chevron-up active'></i>
                </div>
            `)
        }
        $(".produto-descricao .boxs #" + id + " .btn-lerMais").click(function () {
            $(".produto-descricao .boxs #" + id + ' .fa-chevron-up').toggleClass('active');
            $(".produto-descricao .boxs #" + id + ' .fa-chevron-down').toggleClass('active');
            $(".produto-descricao .boxs #" + id + " .texto").addClass("active",1000);
            $(this).css("display", "none");
        })
    })

    let $classe = $('.banner-product2-produto .products ul');
    $classe.find('.helperComplement').remove();
    $classe.not('.slick-initialized').slick({
        infinite: true,
        speed: 600,
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        autoplay: false,
        prevArrow: `<div class='arrow prev'><i class="lni-arrow-left"></i></div>`,
        nextArrow: `<div class='arrow next'><i class="lni-arrow-right"></i></div>`,
        responsive: [
            {
                breakpoint: 950,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    // MODAL COMPRA
    setTimeout(function () {
        let modal1 = $("#modal-add-produto");
        let modal2 = $("#modal-personalizar");
        let btn1 = $(".buy-in-page-button");
        let btn2 = $("#btn-personalize");
        let btnClose1 = $("#btn-close-modal-1");
        let btnClose2 = $("#btn-close-modal-2");
        var btnCancel2 = $("#btn-cancel-personalizar");

        btn1.click(function () {
            $(modal1).css('display', 'block');
        })
        btnClose1.click(function () {
            $(modal1).css('display', 'none');
        })
        btn2.click(function () {
            $(modal2).css('display', 'block');
        })
        btnClose2.click(function () {
            $(modal2).css('display', 'none');
            $('input#texto-gravacao').val('');
            $('#valorGravacao').text('');
        })
        btnCancel2.click(function () {
            $(modal2).css('display', 'none');
            $('input#texto-gravacao').val('');
            $('#valorGravacao').text('');
        })
    }, 50);
    let skuBestPriceModal = $('#modal-personalizar .skuBestPrice').text();
    let DivBestPriceModal = $('#modal-personalizar .price-best-price');
    DivBestPriceModal.text(skuBestPriceModal + " à vista");

    window.alert = function () { };


    $('#modal-add-produto .text-topic h1').click(function () {
        let id = $(this).attr('id');
        $('#modal-add-produto #' + id + ' .conteudo').slideToggle();
    })

    function clickPosicaoGravacao(posicao, imgTrue, imgFalse, maxlength, local) {
        $(posicao).click(function () {
            const gravImg = $(imgTrue);
            const gravImgTampa = $(imgFalse)
            const valorGravacao = $('#valorGravacao');

            gravImg.css('display', 'block');
            gravImgTampa.css('display', 'none');
            valorGravacao.removeClass('clip');
            valorGravacao.addClass(local);

            $('#valorGravacao').text('');
            $('input#texto-gravacao').val('');
            $('input#texto-gravacao').attr('maxlength', maxlength);
        })
    }

    $('#escolha-tipografia .grav-input input').click(function(){
        const id = $(this).attr('id');
        const valorGravacao = $('#valorGravacao');
        valorGravacao.removeClass('fonte01');
        valorGravacao.removeClass('fonte02');
        valorGravacao.removeClass('fonte03');
        valorGravacao.addClass(id);
    })

    // Tampa da caneta
    clickPosicaoGravacao('.grav-input #tampa', '#grav-img', '#grav-img-clip', 13, null);

    // Clip da caneta
    clickPosicaoGravacao('.grav-input #clip', '#grav-img-clip', '#grav-img', 3, 'clip');

    $('input#texto-gravacao').on('keyup',function(){
        $('#valorGravacao').text(($(this).val()));
    })
}

export default produto;
