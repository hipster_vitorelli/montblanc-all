import $ from 'jquery';

function termosTextosLegais() {
    $('.text-topic').click(function () {
        var id = $(this).attr('id');
        $('#' + id + ' p').slideToggle();
        $('#' + id + ' .fa-chevron-up').toggleClass('active');
        $('#' + id + ' .fa-chevron-down').toggleClass('active');
    })

    $('.termos-titles .title').click(function () {
        var id = $(this).attr('id');
        $('.title').removeClass('active');
        $('#' + id).addClass('active');
        $('.termos-contents .text').removeClass('active');
        $('.termos-contents #' + id).addClass('active');
        $('#' + id + ' .fa-chevron-down').removeClass('active');
        $('#' + id + ' .fa-chevron-up').addClass('active');
        $('#' + id + ' p').hide();
    })
}

export default termosTextosLegais;
