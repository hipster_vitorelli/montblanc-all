import $ from 'jquery';

function vitrine() {
    $('.filtro .navigation-tabs .menu-departamento .search-single-navigator').prepend('<span class="voltar">Voltar</span>')
    $('.filtro .navigation-tabs .menu-departamento .search-single-navigator').prepend('<i class="lni-arrow-left voltar"></i>')

    $('.searchResultsTime .resultado-busca-numero .label').text('Produtos');

    $('.resultado-busca-filtro .orderBy select option').first().text('');

    $('.resultado-busca-filtro .orderBy').click(function(){
        $(this).toggleClass('active');
    })
}

export default vitrine;
