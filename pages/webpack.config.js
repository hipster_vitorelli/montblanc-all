const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const path = require('path');
module.exports = {
    entry: ["@babel/polyfill/noConflict", path.resolve(__dirname, 'src', 'js', 'main.js')],
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'montblancPagesV2.js',
    },
    devServer: {
        contentBase: path.resolve(__dirname, 'public'),
        port: 8080
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            }
        ]
    },
    optimization: {
        minimizer: [
          new UglifyJsPlugin({
            test: /\.js(\?.*)?$/i,
            exclude: /\/node_modules/,
            parallel: 4,
            sourceMap: true
          })
        ]
      }
}
